import React, { Component } from 'react'
import LibeMap from '../LibeMap'

export default class ParisPopMap extends Component {
  render () {
    const { props } = this

    /* Inner logic */
    const placesDom = props.data.map((place, i) => <li key={i}>
      {place.name}
    </li>)

    return <div>
      <LibeMap />
      <div>Other stuff here</div>
    </div>

    /*return <div>
      <button onClick={e => props.navigateTo('intro')}>Intro</button>
      <ul>{placesDom}</ul>
    </div>*/
  }
}
