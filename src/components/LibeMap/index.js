import React, { Component } from 'react'
import L from 'leaflet'
import { Map, Marker, Popup, TileLayer, Circle, CircleMarker } from 'react-leaflet'

// 'https://www.iizcat.com/uploads/2017/01/zmwe8-fc1.jpg'
// style={{height: '800px', width: '1400px'}}

export default class LibeMap extends Component {
  render () {
    return <div className='libe-map'>
      <Map
        style={{width: '100%', height: '100%'}}
        center={[48.85, 2.3]} zoom={13}>
        <TileLayer
          url="https://{s}.tile.osm.org/{z}/{x}/{y}.png"
          attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors" />
        <CircleMarker center={[48.87, 2.35]} />
        <Circle center={[48.85, 2.3]} fillColor='blue' radius={200} />
        <Marker position={[48.85, 2.3]}>
          <Popup>A pretty CSS3 popup.<br />Easily customizable.</Popup>
        </Marker>
      </Map>
    </div>
  }
}
