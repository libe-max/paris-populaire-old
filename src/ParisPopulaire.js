import React, { Component } from 'react'
import moment from 'moment'
import qs from 'querystring'
import parseTsv from './utils/parse-tsv'
import ParisPopMap from './components/ParisPopMap'
// [WIP] This style should be added in public/index.html, and loaded from
// http://www.liberation.fr/apps/static/styles/paris-populaire.css
import './reset.css'
import './style.css'

export default class ParisPopulaire extends Component {
  constructor (props) {
    super()
    const getParams = qs.parse(window.location.search.slice(1))
    const placeParam = parseInt(getParams.place, 10)
    this.getData = this.getData.bind(this)
    this.navigateTo = this.navigateTo.bind(this)
    this.activatePlace = this.activatePlace.bind(this)
    this.getData(props.spreadsheet)
    const publicPath = process.env.NODE_ENV !== 'development' ? props.public_location.pathname : '/'
    window.history.replaceState('', '', publicPath)
    this.state = {
      config: props,
      fetching: true,
      fetch_error: false,
      data: null,
      page: !isNaN(placeParam) ? 'map' : 'intro',
      active_place: !isNaN(placeParam) ? placeParam : null
    }
  }

  navigateTo (val) {
    this.setState({ page: val })
  }

  activatePlace (val) {
    const parsedVal = parseInt(val, 10)
    if (isNaN(parsedVal)) return
    alert(`activate ${parsedVal}`)
    return this.setState({
      page: 'map',
      active_place: parsedVal
    })
  }

  async getData (source) {
    const response = await fetch(source)
    if (!response.ok) return this.setState({
      fetching: false,
      fetch_error: response.statusText
    })
    const tsv = await response.text()
    const parsed = parseTsv({
      tsv, keysLinePos: 2, types: {
        district: 'number',
        lat: val => val ? parseFloat(val, 10) : Math.random() / 10 + 48.8,
        lon: val => val ? parseFloat(val, 10) : Math.random() / 10 + 2.29,
        timespan: val => {
          if (!val) return
          const span = val.split(/[-—–]/).map(chunk => chunk.trim()).map(date => {
            if (date.split('/').length === 1) return moment(date, 'YYYY')
            if (date.split('/').length === 2) return moment(date, 'MM/YYYY')
            if (date.split('/').length === 3) return moment(date, 'DD/MM/YYYY')
            throw new Error(`Invalid date format: ${date}`)
          })
          return span.length > 1 ? span : span[0]
        }
      }
    })
    return this.setState({
      fetching: false,
      data: parsed
    })
  }

  render () {
    const { props, state } = this

    /* Assign classes to component */
    const classes = ['paris-populaire-app']
    if (state.fetching_error) classes.push('paris-populaire-app_fetching-error')
    if (state.fetching) classes.push('paris-populaire-app_fetching')
    if (state.page === 'intro') classes.push('paris-populaire-app_show-intro')
    if (state.page === 'map') classes.push('paris-populaire-app_show-map')

    /* Display */
    return <div className={classes.join(' ')}>
      <div className='paris-populaire-app__intro'>
        Intro <button onClick={e => this.navigateTo('map')}>Map ?</button>
      </div>
      {!state.fetching && !state.fetch_error && state.data
        ? <div className='paris-populaire-app__map'>
          <ParisPopMap
            navigateTo={this.navigateTo}
            activatePlace={this.activatePlace}
            activePlace={state.active_place}
            data={state.data} />
        </div>
        : ''}
    </div>
  }
}
